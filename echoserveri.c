	#include "csapp.h"

void echo(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	size_t n1;
	char buf1[MAXLINE];
	rio_t rio1;
	struct stat buf;
	int fd;
	

	Rio_readinitb(&rio1, connfd);

	while((n1 = Rio_readlineb(&rio1, buf1, MAXLINE)) != 0) {
		//printf("server received %lu bytes, message %s \n ", n1, buf1);
		//Rio_write,n(connfd, buf1, n1);
		
		if(stat(buf1,&buf)<0){
			
			fprintf(stderr, "No existe el archivo %s \n", buf1);

		}else
		{	
			fd = Open(buf1,O_RDONLY,0);
			char mensaje[buf.st_size];
			while((rio_readn(fd, mensaje,buf.st_size)) > 0){
				printf("mensaje: %s ", mensaje);
				Rio_writen(connfd,mensaje,buf.st_size);		
						
			}
			close(fd);
			
		}
	}

}
