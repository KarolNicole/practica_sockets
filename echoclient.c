#include "csapp.h"

int main(int argc, char **argv)
{
	int clientfd, file;
	char *port;
	char *host;
	char *fName;
	char line[MAXLINE];
	rio_t rio;

	if (argc !=4 ) {
		fprintf(stderr, "usage: %s <host> <port> <file name>\n", argv[0]);
		exit(0);
	}

	host = argv[1];
	port = argv[2];
	fName = argv[3];

	clientfd = Open_clientfd(host, port);

	Rio_readinitb(&rio, clientfd);
	
	Rio_writen(clientfd, fName, strlen(fName));

	file = Open("file.txt",O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH | S_IWGRP | S_IRGRP);

	while (Rio_readlineb(&rio, line, MAXLINE) > 0){
		printf("line: %s", line);
		rio_writen(file,line,strlen(line));
		
	}

	Close(file);
	Close(clientfd);
	exit(0);
}

